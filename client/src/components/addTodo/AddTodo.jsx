import "./addTodo.css";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { addTodo } from "../../redux/actions";

const AddTodo = () => {
  const [content, setContent] = useState({});
  const dispatch = useDispatch();

  const handleChange = (e) => {
    setContent((prev) => ({ ...prev, [e.target.id]: e.target.value }));
  };

  const handleClick = (e) => {
    e.preventDefault()
    dispatch(addTodo(content))
    window.location.reload(true);
  };

  return (
    <div className="Todo">
      <input
        type="text"
        className="content-input"
        id="content"
        placeholder="Content"
        onChange={handleChange}
      />
      <button className="button" type="submit" onClick={handleClick}>
        Add
      </button>
    </div>
  );
};

export default AddTodo;
