import "./listTodo.css";
import { useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { deleteTodo, listTodo } from "../../redux/actions";
import Loading from "../../utils/Loading";

const ListTodo = () => {
  const dispatch = useDispatch()
  const navigate = useNavigate();

  const todoList = useSelector((state) => state.todoList)
  const { loading, todo } = todoList

  useEffect(() => {
    dispatch(listTodo());
  }, [dispatch]);

  const handleEdit = (id) => {
    navigate(`/${id}`)
  };

  const handleDelete = (id) => {
    dispatch(deleteTodo(id))
    window.location.reload(true);
  };

  return (
    <>
      {loading ? (
        <Loading />
      ) : (
        <>
          {todo.map((item) => (
            <div className="list" key={item._id}>
              <input type="checkbox" className="checkbox" />
              <p className="content">{item.content}</p>
              <button className="bt" type="submit" onClick={() => handleEdit(item._id)}>
                Edit
              </button>
              <button className="btn" type="submit" onClick={() => handleDelete(item._id)}>
                Delete
              </button>
            </div>
          ))}
        </>
      )}
    </>
  );
};

export default ListTodo;
