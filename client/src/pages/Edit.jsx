import "./edit.css";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { updateTodo } from "../redux/actions";
import { useLocation, useNavigate } from "react-router-dom";

const Edit = () => {
  const [content, setContent] = useState({});
  const location = useLocation();
  const id = location.pathname.split("/")[1];
  const navigate = useNavigate();
  const dispatch = useDispatch();


  const todoUpdate = useSelector((state) => state.todoUpdate);
  const { loading: updateLoading } = todoUpdate;

  const handleClick = (id) => {
    dispatch(updateTodo(id, content));
    navigate("/")
  };

  return (
    <div className="home">
      <h1 className="homeTitle">My Todo App</h1>
        <h3>Back to <a href="#" onClick={handleClick}>home</a></h3>
      <div className="Todo">
        <input
          type="text"
          className="content-input"
          id="content"
          placeholder="Enter new content"
          onChange={(e) => setContent(e.target.value)}
        />
        <button className="button" type="submit" onClick={() => handleClick(id)}>
          Update
        </button>
      </div>
    </div>
  );
};

export default Edit;
