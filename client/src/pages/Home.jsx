import AddTodo from "../components/addTodo/AddTodo";
import ListTodo from "../components/listTodo/ListTodo";
import "./home.css";

const Home = () => {

  return (
    <div className="home">
        <h1 className="homeTitle">My Todo App</h1>
        <AddTodo />
        <ListTodo />
    </div>
  );
};

export default Home;