import { TODO_ADD_FAIL, TODO_ADD_REQUEST, TODO_ADD_SUCCESS, TODO_DELETE_FAIL, TODO_DELETE_REQUEST, TODO_DELETE_SUCCESS, TODO_LIST_FAIL, TODO_LIST_REQUEST, TODO_LIST_SUCCESS, TODO_UPDATE_FAIL, TODO_UPDATE_REQUEST, TODO_UPDATE_SUCCESS } from "./constantes";
import axios from "axios"

// List Todo
export const listTodo = () => async (dispatch) => {
  try {
    dispatch({ type: TODO_LIST_REQUEST })
    const { data } = await axios.get("http://localhost:8000/todo");
    dispatch({ type: TODO_LIST_SUCCESS, payload: data})
  } catch (error) {
    dispatch({
        type: TODO_LIST_FAIL,
        payload: error.response && error.response.data.message ? error.response.data.message : error.message
    })
  }
};

// Add Todo
export const addTodo = (content) => async (dispatch) => {
    try {
        dispatch({ type: TODO_ADD_REQUEST })

        const { data } = await axios.post("http://localhost:8000/todo", { content })

        dispatch({ type: TODO_ADD_SUCCESS, payload: data });
        
        localStorage.setItem("todoInfo", JSON.stringify(data))
    } catch (error) {
        dispatch({
            type: TODO_ADD_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message 
        })
    }
}

// Update Todo
export const updateTodo = (id, content) => async (dispatch) => {
    try {
        dispatch({ type: TODO_UPDATE_REQUEST })

        const { data } = await axios.put(`http://localhost:8000/todo/${id}`, { content });

        dispatch({ type: TODO_UPDATE_SUCCESS, payload: data });
        
        localStorage.setItem("todoInfo", JSON.stringify(data)) 
    } catch (error) {
        dispatch({
            type: TODO_UPDATE_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message 
        })
    }
}

// Delete Todo
export const deleteTodo = (id) => async (dispatch) => {
    try {
        dispatch({ type: TODO_DELETE_REQUEST })

        const { data } = await axios.delete(`http://localhost:8000/todo/${id}`);

        dispatch({ type: TODO_DELETE_SUCCESS, payload: data });
        
        localStorage.removeItem("todoInfo", JSON.stringify(data))
    } catch (error) {
        dispatch({
            type: TODO_DELETE_FAIL,
            payload: error.response && error.response.data.message ? error.response.data.message : error.message 
        })
    }
}