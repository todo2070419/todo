import { TODO_ADD_FAIL, TODO_ADD_REQUEST, TODO_ADD_SUCCESS, TODO_DELETE_FAIL, TODO_DELETE_REQUEST, TODO_DELETE_SUCCESS, TODO_LIST_FAIL, TODO_LIST_REQUEST, TODO_LIST_SUCCESS } from "./constantes";

// List Todo
export const todoListReducer = (state = { todo: [] }, action) => {
    switch (action.type) {
      case TODO_LIST_REQUEST:
        return { loading: true, todo: [] };
      case TODO_LIST_SUCCESS:
        return { loading: false, todo: action.payload };
      case TODO_LIST_FAIL:
        return { loading: false, error: action.payload };
        default: 
          return state
    }
}

// Add Todo
export const todoAddReducer = (state = {}, action) => {
    switch (action.type) { 
      case TODO_ADD_REQUEST:
        return { loading: true };
      case TODO_ADD_SUCCESS:
        return { loading: false, todo: action.payload };
      case TODO_ADD_FAIL:
        return { loading: false, error: action.payload };
      default:
        return state;
    }
}

// Update Todo
export const todoUdapteReducer = (state = {}, action) => {
    switch (action.type) {
      case TODO_ADD_REQUEST:
        return { loading: true };
      case TODO_ADD_SUCCESS:
        return { loading: false, success: true, todo: action.payload };
      case TODO_ADD_FAIL:
        return { loading: false, error: action.payload };
      default:
        return state;
    }
}

// Delete Todo
export const todoDeleteReducer = (state = { todo: [] }, action) => {
    switch (action.type) {
      case TODO_DELETE_REQUEST:
        return { loading: true, todo: [] };
      case TODO_DELETE_SUCCESS:
        return { loading: false, todo: action.payload };
      case TODO_DELETE_FAIL:
        return { loading: false, error: action.payload };
      default:
        return state;
    }
}