import { createStore, combineReducers, applyMiddleware } from "redux"
import thunk from "redux-thunk"
import { composeWithDevTools } from "redux-devtools-extension"
import { todoAddReducer, todoDeleteReducer, todoListReducer, todoUdapteReducer } from "./reducers";

const reducer = combineReducers({
  todoList: todoListReducer,
  todoAdd: todoAddReducer,
  todoUpdate: todoUdapteReducer,
  todoDelete: todoDeleteReducer
});

const initialState = {}

const middleware = [ thunk ]

const store = createStore(
    reducer,
    initialState,
    composeWithDevTools(applyMiddleware(...middleware))
)

export default store