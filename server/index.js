const express = require("express");
const app = express();
const mongoose = require("mongoose");
const cors = require("cors");
const bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");
const todoRoute = require("./routes/todo");
require("dotenv").config();

// mongoDb connection
mongoose.set("strictQuery", false);
mongoose
  .connect(process.env.MONGO_DB, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => console.log("Successful MongoDB connection !"))
  .catch((err) => console.log("Failed MongoDB connection !" + " " + err));


app.use(cors());
app.use(bodyParser.json());
app.use(cookieParser());
app.use(express.json());
app.use("/todo", todoRoute);

// listening server
app.listen(process.env.PORT, () => {
  console.log("Connected successful on port " + process.env.PORT);
});