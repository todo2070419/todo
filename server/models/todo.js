var mongoose = require("mongoose");
var Schema = mongoose.Schema;

const todoSchema = new Schema(
    {
      content: {
        type: String,
        require: true
      }
    },
    {
      timestamps: true,
    }
  ),
  Todo = mongoose.model("Todo", todoSchema);

module.exports = Todo;
