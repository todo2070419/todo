const express = require("express");
const router = express.Router();
const Todo = require("../models/todo")

router.post("/", async (req, res, next) => {
  try {
    const content = req.body.content
    const newTodo = new Todo(content);
    const savedTodo = await newTodo.save();
    res.status(200).json(savedTodo);
  } catch (error) {
    next(error);
  }
});

router.put("/:id", async (req, res, next) => {
  try {
    const updateTodo = await Todo.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      { new: true }
    );
    res.status(200).json(updateTodo);
  } catch (error) {
    next(error);
  }
});

router.delete("/:id", async (req, res, next) => {
  try {
    const deleteTodo = await Todo.findByIdAndDelete(req.params.id);
    res.status(200).send({ message: "Todo has been deleted." });
  } catch (error) {
    next(error);
  }
});

router.get("/", async (req, res, next) => {
  try {
    const todos = await Todo.find().sort({ content: 1 });
    res.status(200).json(todos); 
  } catch (error) {
    next(error);
  }
});

router.get("/:id", async (req, res, next) => {
  try {
    const todo = await Todo.findById({ _id: req.params.id});
    res.status(200).json(todo);
  } catch (error) {
    next(error);
  }
});

module.exports = router;